;Jorgenio

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"
  !include "x64.nsh"
;--------------------------------
;General
  !define MUI_ICON ".\pandasoft.ico"
  ;Name and file
  Name "Pandasoft"

  OutFile "..\Pandasoft-setup.exe"

  ;Default installation folder
  InstallDir "c:\pandasoft"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\Pandasoft" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel user

;--------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Interface Configuration
  !define MUI_WELCOMEFINISHPAGE_BITMAP ".\wizard.bmp"
  !define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP ".\headerLeft.bmp" ; optional
  !define MUI_ABORTWARNING

;--------------------------------
;Pages
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE ".\licencia.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
; pagina ultima
!define MUI_FINISHPAGE_RUN
;!define MUI_FINISHPAGE_RUN_TEXT "Run Npp"
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchPandasoft"

  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Pandasoft" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "Spanish"
  
Function .onInit
        # the plugins dir is automatically deleted when the installer exits
        InitPluginsDir
        File /oname=$PLUGINSDIR\splash.bmp ".\splash.bmp"
        #optional
        #File /oname=$PLUGINSDIR\splash.wav "C:\myprog\sound.wav"



        advsplash::show 1000 600 400 -1 $PLUGINSDIR\splash

        Pop $0          ; $0 has '1' if the user closed the splash screen early,
                        ; '0' if everything closed normally, and '-1' if some error occurred.



        Delete $PLUGINSDIR\splash.bmp
FunctionEnd

Function LaunchPandasoft
  Exec '"$INSTDIR\pandasoft.exe" '
FunctionEnd

;--------------------------------
;Installer Sections
Section "Pandasoft (necesario)" SecCore

  SetOverwrite on
  SetDetailsPrint textonly
  DetailPrint "Instalando Pandasoft..."
  SetDetailsPrint listonly

  SectionIn 1 2 RO
  SetOutPath "$INSTDIR"
  
  File ..\*.*
  
  SetOutPath "$INSTDIR\qt\plugins\sqldrivers"
  
  File ..\qt\plugins\sqldrivers\*.*


SectionEnd

Section "Desinstalador" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Pandasoft" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Desinstalar.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
	CreateShortCut "$DESKTOP\Pandasoft.lnk" "$INSTDIR\pandasoft.exe"
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
	CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Pandasoft.lnk" "$INSTDIR\pandasoft.exe"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Desinstalar.lnk" "$INSTDIR\Desinstalar.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
    LangString DESC_SecCore ${LANG_ENGLISH} "Librerias basicas necesarias."
	LangString DESC_SecDummy ${LANG_ENGLISH} "Crear desinstalador."


  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
	
  Delete "$DESKTOP\Pandasoft.lnk"
  Delete "$INSTDIR\Desinstalar.exe"
  Delete "$INSTDIR\pandasoft.exe"
  Delete "$INSTDIR\libgcc_s_dw2-1.dll"
  Delete "$INSTDIR\mingwm10.dll"
  Delete "$INSTDIR\QtCore4.dll"
  Delete "$INSTDIR\QtGui4.dll"
  Delete "$INSTDIR\QtSql4.dll"
  Delete "$INSTDIR\licencia.txt"
  Delete "$INSTDIR\pandasoft.db"

  Delete "$INSTDIR\qt\plugins\sqldrivers\qsqlite4.dll"
  RMDir "$INSTDIR\qt\plugins\sqldrivers"
  RMDir "$INSTDIR\qt\plugins"
  RMDir "$INSTDIR\qt"
  
  RMDir "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  
  Delete "$SMPROGRAMS\$StartMenuFolder\Pandasoft.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Desinstalar.lnk"
  
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  
  DeleteRegKey /ifempty HKCU "Software\Pandasoft"

SectionEnd

BrandingText "Instalador de Pandasoft"